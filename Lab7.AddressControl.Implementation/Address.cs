﻿using Lab7.AddressControl.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lab7.AddressControl
{
    public class Address : IAddress
    {
        private Control control = new AddresControl();

        public Control Control
        {
            get { return control; }
        }
        //public System.Windows.Controls.Control Control
        //{
        //    get { throw new NotImplementedException(); }
        //}

        public event EventHandler<AddressChangedArgs> AddressChanged;
    }
}

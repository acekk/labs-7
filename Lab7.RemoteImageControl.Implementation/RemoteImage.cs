﻿using Lab7.RemoteImageControl.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lab7.RemoteImageControl.Implementation
{
    public class RemoteImage : IRemoteImage
    {
        private Control control = new RemoteImageControl();

        public Control Control
        {
            get { return control; }
        }
        //public System.Windows.Controls.Control Control
        //{
        //    get { throw new NotImplementedException(); }
        //}

        public void Load(string url)
        {
            MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri(url)));
            ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);
        }
    }
}

﻿using Lab7.AddressControl.Contract;
using Lab7.Infrastructure;
using Lab7.RemoteImageControl.Contract;
using System;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Lab7.Main
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void Window_Initialized(object sender, EventArgs e)
        {
            var container = Configuration.ConfigureApp();

            var address = container.Resolve<IAddress>();
            Grid.SetRow(address.Control, 0);
            var rdef = new RowDefinition();
            rdef.Height = new GridLength(30);
            this.Panel.RowDefinitions.Add(rdef);
            this.Panel.Children.Add(address.Control);

            var image = container.Resolve<IRemoteImage>();
            Grid.SetRow(image.Control, 1);
            this.Panel.RowDefinitions.Add(new RowDefinition());
            this.Panel.Children.Add(image.Control);

            this.Panel.ShowGridLines = true;

            //MemoryStream ms = new MemoryStream(new WebClient().DownloadData(new Uri("http://obrazki.joe.pl/sub_images/roza-serce-kropel-rosy.gif")));
            //ImageSourceConverter imageSourceConverter = new ImageSourceConverter();
            //ImageSource imageSource = (ImageSource)imageSourceConverter.ConvertFrom(ms);

          
           image.Load("http://goes.gsfc.nasa.gov/pub/goes/080913.ike.poster.jpg"); 

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
        }

    }
}
